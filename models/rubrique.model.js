module.exports = (sequelize, Sequelize) => {
    const Rubrique = sequelize.define("rubrique", {
        /*
        techno_id: {
            type: Sequelize.INTEGER
        },
        */
        titre: {
            type: Sequelize.STRING
        },
        contenu: {
            type: Sequelize.TEXT
        }
    })

    return Rubrique
}