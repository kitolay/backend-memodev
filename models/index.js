const dbConfig = require("../config/db.config.js")
const Sequelize = require("sequelize")

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,

    pool: {
        max: dbConfig.max,
        min: dbConfig.min,
        pool: dbConfig.pool,
        acquire: dbConfig.acquire,
        idle: dbConfig.idle
    }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.technos = require("./techno.model.js")(sequelize, Sequelize)
db.rubriques = require("./rubrique.model.js")(sequelize, Sequelize)

db.technos.hasMany(db.rubriques)
db.rubriques.belongsTo(db.technos)

module.exports = db

