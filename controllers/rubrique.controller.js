const db = require("../models")
const Rubrique = db.rubriques
const Techno = db.technos

exports.findAllByTechno = (req, res) => {
    Rubrique.findAll({
        include: [{
            model: Techno,
            required: true
        }]
    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log("Erreur : "+err.message)
    })
}