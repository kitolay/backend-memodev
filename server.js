const express = require('express')
const app = express()
const cors = require('cors')
const db = require("./models")
const summary = require('./data/summary.json')

const rubrique = require("./controllers/rubrique.controller")

app.use(cors())

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded())

// Parse JSON bodies (as sent by API clients)
app.use(express.json())

db.sequelize.sync()
  .then(() => {
    console.log("Synced db.")
  })
  .catch((err) => {
    console.log("Failde to sync DB "+err.message)
  })

app.get("/rubriques", rubrique.findAllByTechno)

app.get('/summary', (req, res) => {
	res.header("Access-Control-Allow-Origin", "*")
    res.status('200').json(summary)
})

// set port, listen for requests
const PORT = process.env.PORT || 8282
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})